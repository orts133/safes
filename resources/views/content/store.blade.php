@extends('master')

@section('content')


<div id="main-container" class="container">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">דף הבית</a></li>
            <li class="active">החנות</li>
    </ol>
        <div class="col-md-3">
            <h4 class="product-head">קטגוריות</h4>
            <div class="list-group categories">
                @if($categories && count($categories)>0) 
                @foreach($categories as $row)
                    <a href="{{url('store/' . $row['url'])}}" class="list-group-item">
                            
                           {{$row['title']}}
                           <i class="glyphicon glyphicon-chevron-left"></i>
                    </a>
                @endforeach
                @endif 
            </div>
        </div>
    
        <div class="col-md-9">
            
            <h4 class="product-head">חנות הכספות</h4>
            <hr class="hr">
            @if($categories && count($categories)>0) 
                @foreach($categories as $row)
            <h2 class="main-heading2">
                <a href="{{url('store/' . $row['url'])}}">{{$row['title']}}</a>
            </h2>
            
            <div class="row cat-intro">
                <div class="col-sm-3">
                        <img src="{{asset('assets/img/' . $row['image']) }}" alt="Image" class="img-responsive img-thumbnail">
                </div>
                <div class="col-sm-9 cat-body">
                        <p>
                        {!!$row['article']!!}      
                        </p>
                </div>
            </div>
            <hr class="hr">
            @endforeach
                @endif 
        </div>
    </div>
</div>

@endsection
