
<div class="modal-header">
    <h4 class="product-head">מלא את הפרטים הבאים לקבלת הצעת מחיר עבור הדגם הנבחר</h4>
</div>

<div class="modal-body">
    <div class="table-responsive compare-table">
            <table class="table table-bordered">
                <thead>
                    <tr>

                        <td class="td-head">דגם</td>
                        <td class="td-head">גובה חיצוני (ס"מ)</td>
                        <td class="td-head">רוחב חיצוני (ס"מ)</td>
                        <td class="td-head">עומק חיצוני (ס"מ)</td>
                        <td class="td-head">גובה פנימי (ס"מ)</td>
                        <td class="td-head">רוחב פנימי (ס"מ)</td>
                        <td class="td-head">עומק פנימי (ס"מ)</td>
                        <td class="td-head">משקל (ק"ג)</td>

                    </tr>
                </thead>
                <tbody>


                <tr>
                    <td><strong id="offer_title">{{$size['title']}}</strong></td>
                    <td id="offer_height">{{$size['o_height']}}</td>
                    <td id="offer_width">{{$size['o_width']}}</td>
                    <td id="offer_depth">{{$size['o_depth']}}</td>
                    <td id="offer_inheight">{{$size['in_height']}}</td>
                    <td id="offer_inwidth">{{$size['in_width']}}</td>
                    <td id="offer_indepth">{{$size['in_depth']}}</td>
                    <td id="offer_weight">{{$size['weight']}}</td>
                </tr>
                </tbody>
            </table>
    </div>
</div>

<div class="modal-footer">
    <div class="panel panel-smart">
         
          <div class="panel-body text-right">
              <form class="form-inline" method="post" action="{{url('store/dataorder')}}" role="form">
                  {!!csrf_field()!!}
                  <div class="form-group">
                          <label for="name" class="col-sm-2 control-label">
                                  שם
                          </label>
                          <div class="col-sm-8">
                                  <input type="text" class="form-control" name="name" id="name" placeholder="שם" value="{{Input::old('name')}}">
                          </div>
                  </div>
                  <div class="form-group">
                          <label for="email" class="col-sm-2 control-label">
                                  אימייל
                          </label>
                          <div class="col-sm-10">
                                  <input type="email" class="form-control" name="email" id="email" placeholder="אימייל" value="{{Input::old('email')}}">
                          </div>
                  </div>
                  <div class="form-group">
                          <label for="phone" class="col-sm-2 control-label">
                                  טלפון
                          </label>
                          <div class="col-sm-10">
                                  <input type="phone" class="form-control" name="phone" id="email" placeholder="טלפון" value="{{Input::old('phone')}}">
                          </div>
                  </div>
                  <div class="form-group">
                          <label for="fax" class="col-sm-2 control-label">
                                  פקס
                          </label>
                          <div class="col-sm-10">
                                  <input type="phone" class="form-control" name="fax" id="email" placeholder="פקס" value="{{Input::old('fax')}}">
                          </div>
                  </div>
                  
                      <div class="form-group pull-left">
                              <div class="col-sm-offset-2 col-sm-12 ">
                                  <input name="id" type="hidden" id="offer_id" />
                                      <input name="submit" type="submit" class="btn btn-block btn-lg  btn-black  send-contact" value="שלח">
                              </div>
                      </div>
                  </form>
          </div>
      </div>
    
</div>



