@extends('master')

@section('content')
<div class="main-container container" style=" background: none;">
    <div class="row">
        <div class="col-sm-11">
            <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">דף הבית</a></li>
                    <li class="active">{{ $title }}</li>
            </ol>
            <div class="page-box">
                <div class="row">
                    @foreach($content as $row)

                    @if($row['image'])
                    <div class="col-sm-4">
                            <img src="{{asset('assets/img/' . $row['image']) }}" alt="image" class="img-responsive">
                    </div>
                    @endif
                    <div class="col-sm-8">
                        <h3>{{$row['title']}}</h3>
                        <p>
                          {!!$row['body']!!}
                        </p>
                    </div>

                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
