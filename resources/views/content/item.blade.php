
@extends('master')

@section('content')

<div id="main-container" class="container">
    <div class="row">
        <div class="col-md-12">
    @if($product)
            <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">דף הבית</a></li>
                    <li><a href="{{url('store/'. $categorie)}}">{{$categorie}}</a></li>
                    <li class="active">{{ $product['title'] }}</li>
            </ol>

            <div class="row product-info">
                <div class="col-sm-5 images-block ">
                    
                    <p>
                        <a class="image-popup-no-margins" href="{{ asset('assets/img/' . $product['image']) }}"  >
                        <img  src="{{ asset('assets/img/' . $product['image']) }}" alt="Image" class="img-responsive thumbnail" />
                        </a>
                        </p>
                  
                </div>
                <div class="col-sm-7 product-details">

                    <h4 class="product-head" style="margin: 0">{{ $product['title'] }}</h4>

                    <ul class="list-unstyled manufacturer">
                        <li>{!! $product['preview']!!}</li>
                        <hr/>
                        <li class="showmore">
                            <span>מפרט:</span><strong> {!! $product['article'] !!} </strong>
                        </li>
                    </ul>

                      

                        

                </div>
            </div>
      
      
     
    @else
    
    <p> <i>אין מוצר...</i> </p>
    
    @endif
     
@if($sizes)
    <h4 class="product-head" style="margin: 0">טבלת מידות</h4>
 </div>
        <div class="col-md-3 images-block" style="margin-top: 30px">
          
            @if($images)
                    
                        @foreach($images as $item)
                     
                            <a class="pull-left image-popup-no-margins" href="{{ asset('assets/img/' . $item['url']) }}" >
                            <img src="{{ asset('assets/img/' . $item['url']) }}" alt="Image" class="img-responsive thumbnail image-side">
                            <p class="text-center">{{$item['title']}}</p>
                            </a>
                        
                      
                        @endforeach    
                
                    @endif
    
        </div>
        <div class="col-md-9" style="margin-bottom: 50px;">
    <div class="table-responsive compare-table">
        <table class="table table-bordered">
            <thead>
                <tr>
                    
                    <td class="td-head">דגם</td>
                    <td class="td-head">גובה חיצוני (ס"מ)</td>
                    <td class="td-head">רוחב חיצוני (ס"מ)</td>
                    <td class="td-head">עומק חיצוני (ס"מ)</td>
                    <td class="td-head">גובה פנימי (ס"מ)</td>
                    <td class="td-head">רוחב פנימי (ס"מ)</td>
                    <td class="td-head">עומק פנימי (ס"מ)</td>
                    <td class="td-head">משקל (ק"ג)</td>
                    <td>פעולה</td>
                </tr>
            </thead>
            <tbody>
                 
                @foreach($sizes as $num)
            <tr>
                <td><strong>{{$num['title']}}</strong></td>
                <td>{{$num['o_height']}}</td>
                <td>{{$num['o_width']}}</td>
                <td>{{$num['o_depth']}}</td>
                <td>{{$num['in_height']}}</td>
                <td>{{$num['in_width']}}</td>
                <td>{{$num['in_depth']}}</td>
                <td>{{$num['weight']}}</td>
                <td>
                <buttun data-id="{{ $num['id'] }}" type="button" title="הוסף לעגלה" class="tool-tip btn btn-block btn-cart good add-to-cart">
                    + <i class="glyphicon glyphicon-shopping-cart "></i>  הוסף לעגלה!
                </buttun><br/>
                
                <buttun data-id="{{ $num['id'] }}" type="button" title="לחץ לקבלת הצעת מחיר עבור הדגם הנבחר" class="tool-tip btn btn-block btn-cart get_offer">
                        <i class="glyphicon glyphicon-check"></i>&nbsp       קבל הצעה!
                </buttun>
                </td>
            </tr>
                @endforeach  
                
            </tbody>
        </table>
    </div>
@endif     


  
</div>
</div>
</div>

<div class="modal fade offer" tabindex="-1" role="dialog"  aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          
          @include('content.offer')
        
      </div>
    </div>
  </div>

@endsection
