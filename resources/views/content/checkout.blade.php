@extends('master')

@section('content')

<div id="main-container" class="container">
                
    <h4 class="product-head">פריטים בעגלה</h4>
    
    
        @if( count($cartCollection) > 0 )
            <div class="table-responsive shopping-cart-table">
                <table class="table table-bordered">
                    <thead class="table-head">
                        <tr>
                            <td class="text-center">דגם</td>							
                            <td class="text-center">כמות</td>
                            <td class="text-center">מידות</td>
                            <td class="text-center">פעולה</td>
                        </tr>
                    </thead>

                
                @foreach($cartCollection as $row)
                
               
                
                <tbody>
                    <tr>
                        <td class="text-center"><strong>{{ $row['attributes'][0] }}</strong></td>							
                            <td class="text-center">
                                <div class="input-group btn-block">
                                    <i data-id="{{$row['id']}}" data-op="minus" type="button" title="הסר / הפחית מוצר" class="update-cart-btn glyphicon glyphicon-minus tool-tip glyph"></i>           
                                    <input type="text" name="quantity" value="{{ $row['quantity'] }}" size="1" class="text-center input-group-sm" />
                                      <i data-id="{{$row['id']}}" data-op="plus" type="button" title="הוסף מוצר" class="update-cart-btn glyphicon glyphicon-plus tool-tip glyph"></i>
                                </div>								
                            </td>
                            <td class="text-right">
                                <ul style="float:right">
                                    <li class="list-group-item list-group-item-info"><strong>גובה חוץ-</strong>{{ $row['attributes'][1] }} ס"מ</li>
                                    <li class="list-group-item list-group-item-info"><strong>רוחב חוץ-</strong>{{ $row['attributes'][2] }} ס"מ</li>
                                </ul>   
                                <ul style="float:right">
                                    <li class="list-group-item list-group-item-info"><strong>עומק חוץ-</strong>{{ $row['attributes'][3] }} ס"מ</li>
                                    <li class="list-group-item list-group-item-info"><strong>גובה פנים-</strong>{{ $row['attributes'][4] }} ס"מ</li>
                                </ul>
                                <ul style="float:right">
                                    <li class="list-group-item list-group-item-info"><strong>רוחב פנים-</strong>{{ $row['attributes'][5] }} ס"מ</li>
                                    <li class="list-group-item list-group-item-info"><strong>עומק פנים-</strong>{{ $row['attributes'][6] }} ס"מ</li>
                                </ul>
                                <ul style="float:right">
                                    <li class="list-group-item list-group-item-info"><strong>משקל-</strong>{{ $row['attributes'][7] }} ק"ג</li>
                                </ul>
                            </td>
                            
                            <td class="text-center">
                                <i data-id="{{$row['id']}}" data-op="remove" title="הסר מוצר" class="update-cart-btn tool-tip glyphicon glyphicon-remove glyph"></i>   
                            </td>
                    </tr>
                </tbody>

                @endforeach


                </table>
            </div>
<div class="col-sm-12">
        <h4 class="product-head good">מלא\י את הפרטים הבאים לקבלת הצעת מחיר משתלמת!</h4>
        <div class="panel panel-smart">
         
          <div class="panel-body">
              <form class="form-inline" method="post" action="{{url('store/order')}}" role="form">
                  
                  {!!csrf_field()!!}
                  <div class="form-group">
                          <label for="name" class="col-sm-2 control-label">
                                  שם
                          </label>
                          <div class="col-sm-8">
                                  <input type="text" class="form-control" name="name" id="name" placeholder="שם" value="{{Input::old('name')}}">
                          </div>
                  </div>
                  <div class="form-group">
                          <label for="email" class="col-sm-2 control-label">
                                  אימייל
                          </label>
                          <div class="col-sm-10">
                                  <input type="email" class="form-control" name="email" id="email" placeholder="אימייל" value="{{Input::old('email')}}">
                          </div>
                  </div>
                  <div class="form-group">
                          <label for="phone" class="col-sm-2 control-label">
                                  טלפון
                          </label>
                          <div class="col-sm-10">
                                  <input type="phone" class="form-control" name="phone" id="email" placeholder="טלפון" value="{{Input::old('phone')}}">
                          </div>
                  </div>
                  <div class="form-group">
                          <label for="fax" class="col-sm-2 control-label">
                                  פקס
                          </label>
                          <div class="col-sm-10">
                                  <input type="phone" class="form-control" name="fax" id="email" placeholder="פקס" value="{{Input::old('fax')}}">
                          </div>
                  </div>
                  
                      <div class="form-group pull-left">
                              <div class="col-sm-offset-2 col-sm-12 ">
                                      <input name="submit" type="submit" class="btn btn-block btn-lg  btn-black  send-contact" value="שלח">
                              </div>
                      </div>
                  </form>
          </div>
      </div>
</div>
        
        @else
        
        <h4 class="h4 text-right">אין מוצרים בעגלה...</h4>
        <a class="text-right" href="{{url('store') }}"><h4>לחנות שלנו >></h4></a>
        
        
        
        
        @endif
                
           
            
        
        
</div>



@endsection
