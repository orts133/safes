


   @if($cart) 
    <li>
        <table class="table hcart">  
        
            <tr>
                <td class="text-center">כותרת</td>
                <td class="text-center">דגם</td>
                <td class="text-center">כמות</td>
                <td class="text-center">פעולה</td>
            </tr>
        
        @foreach($cart as $row)
            <tr>
                
                    <td class="text-left">{{$row['name']}}</td>
                    <td class="text-left">{{$row['attributes'][0] }}</td>
                    <td class="text-center">x{{ $row['quantity'] }}</td>
                    
                    <td class="text-center">
                        <i data-id="{{$row['id']}}" data-op="remove" title="הסר מהעגלה" class="glyph update-cart-btn tool-tip glyphicon glyphicon-remove"></i>
                    </td>
            </tr>  
            @endforeach
            
            
           
        </table>
    </li>
    
    <li>
        
            <p class="text-center btn-block1">
                    <a href="{{ url('store/clear_cart')}}">נקה עגלה</a>
                    <a href="{{ url('store/checkout')}}">סיכום >></a>
            </p>
    </li>
    
    @else
    <li>
        <p class="text-left">אין מוצרים בעגלה...</p>
        <p class="text-center btn-block1">
            <a href="{{ url('store')}}">בקר בחנות!</a>
        </p>
    </li>
    @endif
