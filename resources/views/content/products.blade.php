@extends('master')

@section('content')



<div id="main-container" class="container">
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}">דף הבית</a></li>
        <li><a href="{{url('store')}}">החנות</a></li>
        <li>{{$categorie}}</a></li>
    </ol>
    <div class="col-md-3">
    <h3 class="side-heading">קטגוריות</h3>
        <div class="list-group categories">
            @if($categories && count($categories)>0) 
            @foreach($categories as $row)
                <a href="{{url('store/' . $row['url'])}}" class="list-group-item @if($row['url'] == $categorie) on @endif">
                        
                       {{$row['title']}}
                       <i class="glyphicon glyphicon-chevron-left"></i>
                </a>
            @endforeach
            @endif 
        </div>
    </div>
    <div class="col-md-9">
           
        @if(Session::has('ok'))
        <div class="alert alert-success">
            <span><i class="glyphicon glyphicon-ok"></i> {{Session::get('ok')}} 
            <a href="{{url('store/checkout')}}">Checkout >></a></span>
        </div>
        @endif  
        
        @if(count($products)>0)
        @foreach($products as $row)

        <div class="col-xs-12">
    <div class="product-col list clearfix">
            <div class="image pull-right">
                <a href="{{url('store/' . $categorie . '/' . $row['url'])}}"><img src="{{asset('assets/img/' . $row['image']) }}" alt="product" class="img-responsive"></a>
            </div>
            <div class="caption">
                    <h4><a href="{{url('store/' . $categorie . '/' . $row['url'])}}">{{$row['title']}}</a></h4>
                    <div class="description">
                             {!! $row['preview'] !!}
                    </div>
                  
                    <div class="cart-button button-group pull-left">                         
                        <a href="{{url('store/' . $categorie . '/' . $row['url'])}}" type="button" class="btn btn-black">
                            קרא עוד...
                            <i class="glyphicon glyphicon-chevron-left"></i>   
                        </a>
                    </div>
            </div>
    </div>
</div>
 @endforeach
@endif 


    </div>
</div>
               




@endsection
