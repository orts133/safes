
@extends('master')

@section('content')

<div id="main-container-home" class="container">
	<div class="row"> 
   
                     
            <div class="col-md-9">
                <div id="main-carousel" class="carousel slide" data-ride="carousel">                            
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="{{asset('assets/img/img1.jpg')}}" alt="Slider" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="{{asset('assets/img/img2.jpg')}}" alt="Slider" class="img-responsive">
                        </div>
                    </div>						
                    <a class="left carousel-control" href="#main-carousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#main-carousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>						
                </div>
            </div>
	
        
       
            <div class="col-md-3">
              
                @if($specials && count($specials)>0)
                <h4 class="product-head">במבצע!</h4>
                <ul class="side-products-list">
                   @foreach($specials as $item) 
                    <li class="clearfix">
                        <a href="{{url('store/' . $item['categorie'] . '/' . $item['url'])}}"><img src="{{asset('assets/img/' . $item['image']) }}" alt="product" class="img-responsive"></a>
                        <h3 class="no_margin"><a href="{{url('store/' . $item['categorie'] . '/' . $item['url'])}}">{{$item['title']}}</a></h3>
                        <h4 class="no_margin sale">החל מ- &#8362;{{$item['old_price']}}!</h4>
                    </li>
                    @endforeach
                </ul>
                @endif
            </div>
        </div>
    
<section class="product-list">
   
        @if($latests && count($latests)>0)
        <h2 class="product-head">המוצרים שלנו</h2>
        <div class="row">
            @foreach($latests as $item)
            <div class="col-md-3 col-sm-6"> 
                <div class="product-col">
                        <div class="image">
                            <a href="{{url('store/' . $item['categorie'] . '/' . $item['url'])}}"><img src="{{asset('assets/img/' . $item['image']) }}" alt="product" class="img-responsive"></a>
                        </div>
                        <div class="caption">
                                <h4><a href="{{url('store/' . $item['categorie'] . '/' . $item['url'])}}">{{$item['title']}}</a></h4>
                                <div class="description max">
                                       {!!$item['preview']!!}
                                </div>
                                
                                <div class="cart-button button-group">                         
                    									
                                    <a href="{{url('store/' . $item['categorie'] . '/' . $item['url'])}}" type="button" class="pull-left btn btn-black">
                                        קרא עוד...
                                        <i class="glyphicon glyphicon-chevron-left"></i>   
                                    </a>
                                </div>
                        </div>
                </div>
            </div>
                    
                       
          @endforeach      
        </div>
    @endif    
     
</section>
       
        <h2 class="product-head">צריך עזרה בבחירת כספת?</h2>
            @include('forms.contact')
</div>
 
  



@endsection
