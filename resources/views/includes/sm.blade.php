


<div class="modal fade sm-box" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog">
    <div class="modal-content ">
        <div class="modal-header">
            
              <div class="panel panel-smart">
                  <div class="panel-heading">
                         <h4 class="product-head good"><i class="glyphicon glyphicon-ok"></i> בוצע!</h4>
                         <h3 class="h3">{{Session::get('sm')}}</h3>
                  </div>

              </div>
        
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-black" data-dismiss="modal">סגור</button>
                
            </div>
       
    </div>
  </div>
</div>
