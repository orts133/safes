


<div class="modal fade em-box" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog">
    <div class="modal-content ">
        <div class="modal-header">
            
              <div class="panel panel-smart">
                  <div class="panel-heading">
                         <h4 class="product-head wrong"><i class="glyphicon glyphicon-remove"></i>משהו לא טוב!</h4>
                         <div class="form-group ">
                            <div class="col-sm-3 text-danger"><i class="glyphicon glyphicon-remove"></i>פרטים שגואים:</div>
                            <div class="col-sm-8">
                                <ul class="bg-danger">
                                    @foreach($errors->all() as $error)

                                    <li>{{$error}}</li>

                                    @endforeach
                                </ul>
                            </div>
                        </div>
                  </div>

              </div>
        
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-black" data-dismiss="modal">סגור</button>
                
            </div>
       
    </div>
  </div>
</div>
