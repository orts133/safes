<div class="row">
    <div class="col-sm-4">
        <div class="panel panel-smart">
            <div class="panel-heading">
                <h4 class="product-head">פרטי החברה</h4>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled contact-details">
                    <li class="clearfix">
                        <i class="glyphicon glyphicon-home"></i>
                        <span>
                            <strong>iSafe</strong> <br>
                                {{$address}}
                        </span>
                    </li>
                    <li class="clearfix">

                        <span>
                            <i class="glyphicon glyphicon-phone"></i>
                            <strong>טלפון:</strong> <br>
                                 {{$jobphone}} <br>
                                 {{$cellphone}} <br>
                                 <strong>פקס:</strong> <br>
                                 {{$jobfax}}
                        </span>
                    </li>
                    <li class="clearfix">

                        <span>
                            <i class="glyphicon glyphicon-list"></i>
                            <strong>Email:</strong><br>
                            {{$email}} 


                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
          <div class="panel panel-smart">
              <div class="panel-heading">
                     <h4 class="product-head">יצירת קשר</h4>
              </div>
              <div class="panel-body">
                  <form class="form-horizontal" method="post" action="{{url('contact')}}" role="form">
                      {!!csrf_field()!!}
                      <div class="form-group">
                              <label for="name" class="col-sm-2 control-label">
                                      שם
                              </label>
                              <div class="col-sm-10">
                                      <input type="text" class="form-control" name="name" id="name" placeholder="שם" value="{{Input::old('name')}}">
                              </div>
                      </div>
                      <div class="form-group">
                              <label for="email" class="col-sm-2 control-label">
                                      אימייל
                              </label>
                              <div class="col-sm-10">
                                      <input type="email" class="form-control" name="email" id="email" placeholder="אימייל" value="{{Input::old('email')}}">
                              </div>
                      </div>
                      <div class="form-group">
                              <label for="phone" class="col-sm-2 control-label">
                                      טלפון
                              </label>
                              <div class="col-sm-10">
                                      <input type="phone" class="form-control" name="phone" id="email" placeholder="טלפון" value="{{Input::old('phone')}}">
                              </div>
                      </div>
                      <div class="form-group">
                              <label for="subject" class="col-sm-2 control-label">
                                      נושא 
                              </label>
                              <div class="col-sm-10">
                                      <input type="text" class="form-control" name="subject" id="subject" placeholder="נושא" value="{{Input::old('subject')}}">
                              </div>
                      </div>
                          <div class="form-group">
                                  <label for="message" class="col-sm-2 control-label">
                                          הודעה
                                  </label>
                                  <div class="col-sm-10 ">
                                          <textarea name="message" id="message" class="form-control" rows="5" placeholder="תוכן ההודעה" value="{{Input::old('message')}}"></textarea>
                                  </div>
                          </div>
                          <div class="form-group ">
                                  <div class="col-sm-offset-2 col-sm-10 ">
                                          <input name="submit" type="submit" class="btn btn-black send-contact" value="שלח">
                                  </div>
                          </div>
                      </form>
                  

              </div>
          </div>
    </div>
</div>