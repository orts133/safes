@extends('master')

@section('content')
<div id="main-container" class="container">
		<ol class="breadcrumb">
			<li><a href="{{url('/')}}">Home</a></li>
			<li class="active">Register</li>
		</ol>
		<h2 class="main-heading text-left">
			Register <br>
			<span>Create New Account</span>
		</h2>
                <hr class="hr">
		<section class="registration-area">
			<div class="row">
				<div class="col-sm-6">
				<!-- Registration Block Starts -->
					<div class="panel panel-smart">
						<div class="panel-heading">
							<h3 class="panel-title">Personal Information</h3>
						</div>
						<div class="panel-body">
						<!-- Registration Form Starts -->
                                                <form class="form-horizontal" role="form" action="{{url('user/register')}}" method="post">
                                                        {!!csrf_field()!!}
                                                        <div class="form-group">
                                                           
                                                            <div class="form-group">
                                                                <label for="fname lname" class="col-sm-3 control-label">Name :</label>
                                                                <div class="col-sm-4">
                                                                    <input name="fname" type="text" class="form-control" id="fname" placeholder="First Name" value="{{Input::old('fname')}}">
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input name="lname" type="text" class="form-control" id="lname" placeholder="Last Name" value="{{Input::old('lname')}}">
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label for="inputFname" class="col-sm-3 control-label">Email :</label>
                                                                <div class="col-sm-8">
                                                                    <input name="email" type="text" class=" form-control" id="email" placeholder="Email Address" value="{{Input::old('email')}}">
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label for="inputFname" class="col-sm-3 control-label">Password :</label>
                                                                <div class="col-sm-8">
                                                                    <input name="password" type="password" class="form-control" id="password" placeholder="Password">
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label for="inputFname" class="col-sm-3 control-label">Re-Password :</label>
                                                                <div class="col-sm-8">
                                                                    <input name="repassword" type="password" class="form-control" id="repassword" placeholder="Re-Password">
                                                                </div>
                                                            </div>
                                                        
                                                            <div class="col-sm-offset-3 col-sm-9">
                                                                <input name="submit" type="submit" class="btn btn-black" value="Register">
                                                            </div>
                                                            <br>
                                                            <br>
                                                            
                                                            @if( $errors->any() )
                                                            <div class="form-group">
                                                                <div class="col-sm-3 text-danger text-right"><i class="glyphicon glyphicon-remove"></i>Incorrect fields :</div>
                                                                <div class="col-sm-8">
                                                                    <ul class="bg-danger">
                                                                        @foreach($errors->all() as $error)

                                                                        <li>{{$error}}</li>

                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            @endif
                                                            
                                                            
                                                        </div>
							
                                                    </form>
						</div>							
					</div>
				<!-- Registration Block Ends -->
				</div>
				<div class="col-sm-6">
				<!-- Guest Checkout Panel Starts -->
					<div class="panel panel-smart">
						<div class="panel-heading">
							<h3 class="panel-title">
								Checkout as Guest
							</h3>
						</div>
						<div class="panel-body">
							<p>
								Checkout as a guest instead!
							</p>
							<button class="btn btn-black">As Guest</button>
						</div>
					</div>
				
					<div class="panel panel-smart">
						<div class="panel-heading">
							<h3 class="panel-title">
								Already have an acount?
							</h3>
						</div>
						<div class="panel-body">
							<p>
								Login here!
							</p>
							<a href="{{url('user/login')}}" class="btn btn-black">
                                                            Login
                                                        </a>
						</div>
					</div>				
				</div>
			</div>
		</section>
	</div>
@endsection