@extends('master')

@section('content')

<div id="main-container" class="container">
	<!-- Breadcrumb Starts -->
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}">דף הבית</a></li>
        <li class="active">כניסה</li>
    </ol>
<!-- Breadcrumb Ends -->
<!-- Main Heading Starts -->
    <h2 class="main-heading text-right">
            כניסה לחשבון
    </h2>
    <hr class="hr">
    <section class="login-area">
        <div class="row">
            @if(Session::has('log'))
            <h4><i class="glyphicon glyphicon-alert"></i> {{Session::get('log')}}</h4>
                @endif
            <div class="col-sm-6"> 
                
                <div class="panel panel-smart">
                    <div class="panel-heading">
                        <h3 class="panel-title">כניסה</h3>
                            </div>
                            <div class="panel-body">
                                <p>
                                פרטי החשבון
                                </p>
                                <form class="form-inline" role="form" action="{{url('user/login')}}" method="post">
                                            {!!csrf_field()!!}
                                            @if($ds)<input type="hidden" name="ds" value="{{$ds}}">@endif
                                    <div class="form-group">
                                            <label class="sr-only" for="email">אימייל</label>
                                            <input name="email" type="text" class="form-control" id="email" placeholder="אימייל" value="{{Input::old('email')}}">
                                    </div>
                                    <div class="form-group">
                                            <label class="sr-only" for="password">סיסמא</label>
                                            <input name="password" type="password" class="form-control" id="password" placeholder="סיסמא">
                                    </div>
                                    <input name="submit" type="submit" class="btn btn-black" value="כנס">
                                    
                                    @if(Session::get('em'))
                                    <br>
                                    <p class="text-danger"><i class="glyphicon glyphicon-remove"></i> {{Session::get('em')}}</p>
                                    @endif
                                    @if( $errors->any() )
                                    @foreach($errors->all() as $error)
                                    <p class="text-danger"><i class="glyphicon glyphicon-remove"></i> {{$error}}</p>
                                    @endforeach
                                    @endif
                                    
                                    
                                    
                                </form>
                            </div>
                    </div>
            </div>
            
    </div>
    </section>
	
</div>
@endsection
