<!doctype html>
<html lang="he">
    <head>
        
        <meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
        
        <title>{{$title}}</title>
        
<script> var BASE_URL = "{{ url('') }}/"; </script>
       
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/bootstrap/css/bootstrap.min.css')}}" media="screen,projection">
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/bootstrap/css/bootstrap-theme.min.css')}}" media="screen,projection">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-rtl.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">


</head>
<body>

    
<header id="header-area">
    <div class="header-top">
	<div class="container">					
            <div class="col-sm-6 col-xs-12">
		<div class="header-links">
                    <ul class="nav navbar-nav">
                        <li>
                        <a href="{{url()}}">
                            <i class="glyphicon glyphicon-home" title="Home"></i>
                        <span>דף הבית</span>
                        
                        </a>
                        </li>
                        
                        <li>
                        <a href="{{url('store')}}">
                            <i class="glyphicon glyphicon-tag" title="Shop"></i>
                        <span>החנות שלנו</span>
                        
                        </a>
                        </li>
                        
                        <li>
                        <a href="{{url('store/checkout')}}">  
                        <i class="glyphicon glyphicon-shopping-cart" title="Shopping Cart"></i>
                        <span>עגלת קניות:</span>
                        
                        <span class="total-q">{{Cart::getTotalQuantity()}}</span>
                        
                        </a>
                        </li>
                        
                        <li>
                        <a>
                        <i class="glyphicon glyphicon-phone-alt" title="Shopping Cart"></i>
                        <span class="contacts" type="button"  data-toggle="modal" data-target=".contact">צור קשר</span>
                        </a>
                        </li>
                        
                        
                        @foreach($main_menu as $item)
                        <li>
                        <a href="{{url($item['url'])}}">                            
                        <span>{{$item['link']}}</span>                        
                        </a>
                        </li>
                        @endforeach
                        
                        
                    </ul>
                </div>
            </div>

            
            <div class="col-sm-6 col-xs-12">
              <ul class="nav navbar-nav pull-left">
                        <li>
                            <h1 class="top-phone">{{$cellphone}} / {{$jobphone}}</h1> 
                        </li>
                    
                    
                    @if(Session::has('is_admin'))
                    <li>
                    <a>
                    <span>ברוך הבא {{Session::get('user_name')}}!</span>
                    </a>
                    </li>
                    
                    <li>
                    <a href="{{url('user/logout')}}">
                    <i class="glyphicon glyphicon-log-out" title="Logout"></i>
                    <span>יציאה מהחשבון</span>
                    </a>
                    </li>
                    
                    <li>
                    <a href="{{url('cms/dashboard')}}">
                    <span>CMS</span>
                    </a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
       </div>                 
       
        
    <div class="container">
        <div class="main-header">
            <div class="row">					
                
                
                <div class="col-md-3">
                    <div id="cart" class="btn-group btn-block ">
                        <button name="btn" type="button" data-toggle="dropdown" class="btn btn-block btn-lg dropdown-toggle ">
                            <i class="glyphicon glyphicon-shopping-cart"></i>
                            <span class="hidden-md ">בעגלה:</span> 
                            <span id="cart-total">{{Cart::getTotalQuantity()}} מוצר(ים) </span>
                            <i class="glyphicon glyphicon-chevron-down"></i>
                        </button>
                        
                        <ul class="dropdown-menu pull-right">
                        @include('content.cart')
                        </ul> 
                    </div>
                    <h3 class="h3 cart-head">הוסף פריטים לעגלה לקבלת הצעת מחיר משתלמת!
                     
                    </h3>
                    
                </div>
                
                <div class="col-md-9">
                    <div id="logo" >
                        <a href="{{url()}}"><img src="{{ asset('assets/img/logo.png') }}" title="IsraSafe" alt="IsraSafe" class="img-responsive pull-left" /></a>
                        
                    </div>
                   
                </div>
            </div>
        </div>
     </div>
            <nav id="main-menu" class="navbar" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="btn btn-navbar navbar-toggle pull-right" data-toggle="collapse" data-target=".navbar-cat-collapse">
                        
                        <span class="sr-only">Toggle Navigation</span>
                        <i class="glyphicon glyphicon-menu-hamburger"></i>
                        
                    </button>
                    <h3 class="navbar-toggle pull-right">קטגוריות:</h3>
                </div>
                <div class="container">
                    <div class="collapse navbar-collapse navbar-cat-collapse">
                    @include('content.categories')
                    </div>
                </div>
            </nav>   
        
        
   
</header>
             
                
                <div class="modal fade contact" tabindex="-1" role="dialog"  aria-labelledby="myLargeModalLabel">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          @include('forms.contact')
                      </div>
                    </div>
                  </div>
  
    
       
    <div class="main-container"> @yield('content')</div>
  

<footer id="footer-area">
    <div class="footer-links">
        <div class="container">
            <div class="col-md-2 col-sm-4 col-xs-12">
                    <h5>מידע</h5>
                    <ul>
                            @foreach($main_menu as $item)
                            <li><a href="{{url($item['url'])}}">{{$item['link']}}</a></li>
                            @endforeach
                    </ul>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-12">
                    <h5>עקבו אחרינו</h5>
                    <ul>
                            <li><a href="#">פייסבוק</a></li>
                            <li><a href="#">יוטיוב</a></li>

                    </ul>
            </div>


            <div class="col-md-4 col-sm-8 col-xs-12 last">
                <h5 class="pull-left">
                    צור קשר:
                </h5>   
            </div>
            
            <div class="col-md-4 col-sm-8 col-xs-12 last">
                <h4 class="lead">
                    טל: <span>{{$cellphone}}, {{$jobphone}}</span>
                </h4>
                
                <h4 class="lead">
                    אימייל: <span>{{$email}}</span>
                </h4>
                <h4 class="lead">
                    כתובת: <span>{{$address}}</span>
                </h4>
            </div>
        </div>
    </div>

    <div class="copyright">
        <div class="container">
            <p class="pull-left">
                     © {{date('Y')}} IsraSafe &amp; Safes Store. 
            </p>
        </div>
    </div>
</footer>

        @if(Session::has('sm')) @include('includes.sm')  @endif
            @if( $errors->any())@include('includes.em') @endif 
      
        
        
        
        <script type="text/javascript" src="{{asset('assets/js/jquery-1.11.3.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/jquery-migrate-1.2.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/lib/bootstrap/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/bootstrap-hover-dropdown.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/jquery.magnific-popup.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/script.js')}}"></script>
         
    

    </body>
</html>
