

<div class="container">
    <div class="row ">
        
        <div class="btn btn-danger btn-lg btn-block sm-box">
            @foreach($errors->all() as $error)                                          
            <i class="glyphicon glyphicon-remove"></i> {{$error}}
             @endforeach 
             <button type="button" class="btn btn-default">OK</button>
        </div>
    </div>

</div>