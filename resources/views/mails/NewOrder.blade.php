<!doctype html>
<html lang="he">
    <head>
        <meta charset="utf-8">
    </head>
    <body style="text-align: right">
        <h1>הזמנת הצעת מחיר חדשה מהאתר</h1>
        <h3>הלקוח הבא ביקש הצעת מחיר עם הנתונים הבאים:</h3>
        <p><strong>שם: </strong><br> {{$name}}</p>
        <p><strong>אימייל: </strong><br> {{$email}}</p>
        <p><strong>טלפון: </strong><br> {{$phone}}</p>
        <p><strong>פקס:</strong><br> {{$fax}}</p>
        <p>
            <strong>הצעת מחיר על המוצרים:</strong><br> 
            @foreach($orders as $item)
        <ul>
            <li>מוצר: {{$item['name']}}</li>
            <li>דגם: {{$item['attributes'][0]}}</li>
        </ul>
            @endforeach
        </p>
    </body>
</html>