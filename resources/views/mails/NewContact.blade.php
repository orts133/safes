<!doctype html>
<html lang="he">
    <head>
        <meta charset="utf-8">
    </head>
    <body style="text-align: right">
        <h1>בקשת יצירת קשר חדשה מהאתר</h1>
        <h3>הלקוח הבא ביקשר ליצור קשר עם הנתונים הבאים:</h3>
        <p><strong>שם: </strong><br> {{$name}}</p>
        <p><strong>אימייל: </strong><br> {{$email}}</p>
        <p><strong>טלפון: </strong><br> {{$phone}}</p>
        <p><strong>נושא: </strong><br> {{$subject}}</p>
        <p><strong>הודעה: </strong><br> {{$bodyMessage}}</p>
    </body>
</html>