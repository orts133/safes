@extends('cms.main')

@section('cms_content')

<div class="col-md-12">
  
  <h3>דף ההזמנות</h3>
  
  <br>
  
  <div class="col-md-8">
    
  @if(count($orders) > 0)
  
  <table class="table">
    
    <th>שם</th>
    <th>אימייל</th>
    <th>טלפון</th>
    <th>פקס</th>
    <th>תאריך</th>
    <th>הזמנה</th>
    
    @foreach($orders as $row)
    
    <span style="display: none" >{{ $total = 0 }}</span>
    
    <tr>
      
      <td>{{ $row->name }}</td>
      <td>{{ $row->email }}</td>
      <td>{{ $row->phone }}</td>
      <td>{{ $row->fax }}</td>
      <td>{{ $row->created_at }}</td>
      
      
      
      <td>
        
        <ul>
        
        @foreach( json_decode($row->data) as $item )
        
         <li> {{ $item->name }} | {{ implode(',',$item->attributes) }} | כמות: {{ $item->quantity }}  </li>
        
        @endforeach
        
        </ul>
        
      </td>
      
      
    </tr>
    
    @endforeach
    
  </table>
  
  </div>
  
  @else
  
  <p><i>No orders...</i></p>
  
  @endif
  
</div>

@endsection