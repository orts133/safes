
@extends('cms.main')

@section('cms_content')

  <div class="col-md-4">

    <h3>Add new product -  </h3>
    
    @if( count($categories) > 0 )

    <form method="post" action="{{ url('cms/products') }}" enctype="multipart/form-data">

      {!! csrf_field() !!}
      <input type="hidden" value="-1" name="default_categorie">

      <div class="form-group">
          <label for="title">Title:</label>
          <input value="{{ Input::old('title') }}" type="text" class="form-control" name="title"  placeholder="Title">
      </div>
      
      
      
      <div class="form-group">
        
        <label for="categorie">Categorie:</label>
        
        <select name="categorie" class="form-control">
          
          <option value="-1">Choose categorie...</option>
          
          @foreach($categories as $row)
          
            <option value="{{ $row['id'] }}">{{ $row['title'] }}</option>
          
          @endforeach
          
        </select>
        
      </div>
      
      <div class="form-group">
          <label for="preview">Preview Article:</label>
          <textarea name="preview" class="form-control" rows="5">{{ Input::old('preview') }}</textarea>
      </div>
      
      <div class="form-group">
          <label for="article">Article:</label>
          <textarea name="article" class="form-control" rows="10">{{ Input::old('article') }}</textarea>
      </div>
      
      <div class="form-group">
        <label for="image">Product image:</label>
        <input type="file" name="image">
      </div>

      <input type="submit" name="submit" value="Add product" class="btn btn-primary">
      <a class="btn btn-default" href="{{ url('cms/products') }}">Cancel</a>
      <br><br><br><br>
    </form>
    
    @else
    
    <p><i>No categories...</i></p>
    
    @endif
    

  </div>

@endsection