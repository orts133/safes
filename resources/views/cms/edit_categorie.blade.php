
@extends('cms.main')

@section('cms_content')

  <div class="col-md-4">

    <h3>Edit this category -  </h3>

    <form method="post" action="{{ url('cms/categories/' . $categorie['id']) }}" enctype="multipart/form-data">

      {!! csrf_field() !!}
      
      <input type="hidden" name="_method" value="PATCH">
      <input type="hidden" name="id" value="{{ $categorie['id'] }}">
      
      
      <div class="form-group">
          <label for="title">Title:</label>
          <input value="{{ $categorie['title'] }}" type="text" class="form-control" name="title"  placeholder="Title">
      </div>
      
      <div class="form-group">
          <label for="article">article:</label>
          <textarea name="article" class="form-control" rows="10">{!! $categorie['article'] !!}</textarea>
      </div>
      
      <div class="form-group">
        <label for="file">Categorie image:
          <img border="0" width="60" src="{{ asset('assets/img/' . $categorie['image']) }}">
        </label>
        <input type="file" name="image">
      </div>

      <input type="submit" name="submit" value="Save category" class="btn btn-primary">
      <a class="btn btn-default" href="{{ url('cms/categories') }}">Cancel</a>

    </form>

  </div>

@endsection