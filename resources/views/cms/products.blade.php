@extends('cms.main')

@section('cms_content')

<div class="col-md-12">
  
  <h3>כאן עורכים את המוצרים באתר</h3>
  
  <p><a class="btn btn-primary" href="{{ url('cms/products/create') }}">הוספף מוצר חדש</a></p>
  <br>
  
  @if( count($products) > 0 )
  
  <div class="col-md-6">
    <table class="table">

      <th>כותרת</th>
      <th>פעולה</th>

      @foreach($products as $row)

      <tr>

        <td>{{ $row['title'] }}</td>

        <td>
          <a href="{{ url('cms/products/' . $row['id'] . '/edit') }}">ערוך</a> | 
          <a href="{{ url('cms/products/' . $row['id']) }}">מחק</a>
        </td>

      </tr>

      @endforeach

    </table>
  </div>
  
  @endif
  
</div>

@endsection