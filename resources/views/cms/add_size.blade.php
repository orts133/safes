
@extends('cms.main')

@section('cms_content')

  <div class="col-md-4">

    <h3>Add new size -  </h3>
    
    @if( count($products) > 0 )

    <form class="form-inline" method="post" action="{{ url('cms/sizes') }}" enctype="multipart/form-data">

      {!! csrf_field() !!}
      <input type="hidden" value="-1" name="default_categorie">

      <div class="form-group">
          <label for="title">דגם:</label>
          <input value="{{ Input::old('title') }}" type="text" class="form-control" name="title"  placeholder="דגם">
      </div>
      
      
      
      <div class="form-group">
        
        <label for="product_id">מוצר:</label>
        
        <select name="product_id" class="form-control">
          
          <option value="-1">בחר מוצר...</option>
          
          @foreach($products as $row)
          
            <option value="{{ $row['id'] }}">{{ $row['title'] }}</option>
          
          @endforeach
          
        </select>
        
      </div>
      
      <div class="form-group">
          <label for="o_height">גובה חיצוני</label>
          <input value="{{ Input::old('o_height') }}" type="text" class="form-control" name="o_height" >
      </div>
      
      <div class="form-group">
          <label for="o_width">אורך חיצוני</label>
          <input value="{{ Input::old('o_width') }}" type="text" class="form-control" name="o_width" >
      </div>
      
      <div class="form-group">
          <label for="o_depth">עומק חיצוני</label>
         <input value="{{ Input::old('o_depth') }}" type="text" class="form-control" name="o_depth">
      </div>
      
      <div class="form-group">
          <label for="in_height">גובה פנימי</label>
          <input value="{{ Input::old('in_height') }}" type="text" class="form-control" name="in_height">
      </div>
      
      <div class="form-group">
          <label for="in_width">אורך פנימי</label>
          <input value="{{ Input::old('in_width') }}" type="text" class="form-control" name="in_width" >
      </div>
      
      <div class="form-group">
          <label for="in_depth">עומק פנימי</label>
          <input value="{{ Input::old('in_depth') }}" type="text" class="form-control" name="in_depth">
      </div>
      
      <div class="form-group">
          <label for="weight">משקל</label>
          <input value="{{ Input::old('weight') }}" type="text" class="form-control" name="weight">
      </div>
      

      <input type="submit" name="submit" value="Add product" class="btn btn-primary">
      <a class="btn btn-default" href="{{ url('cms/sizes') }}">ביטול</a>
      <br><br><br><br>
    </form>
    
    @else
    
    <p><i>No categories...</i></p>
    
    @endif
    

  </div>

@endsection