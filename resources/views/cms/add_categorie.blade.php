
@extends('cms.main')

@section('cms_content')

  <div class="col-md-4">

    <h3>Add new category -  </h3>

    <form method="post" action="{{ url('cms/categories') }}" enctype="multipart/form-data">

      {!! csrf_field() !!}

      <div class="form-group">
          <label for="title">Title:</label>
          <input value="{{ Input::old('title') }}" type="text" class="form-control" name="title"  placeholder="Title">
      </div>
      
      <div class="form-group">
          <label for="article">article:</label>
          <textarea name="article" class="form-control" rows="10">{{ Input::old('article') }}</textarea>
      </div>
      
      <div class="form-group">
        <label for="file">Categorie image:</label>
        <input type="file" name="image">
      </div>

      <input type="submit" name="submit" value="Add categorie" class="btn btn-primary">
      <a class="btn btn-default" href="{{ url('cms/categories') }}">Cancel</a>

    </form>

  </div>

@endsection