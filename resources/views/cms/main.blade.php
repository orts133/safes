<!doctype html>
<html>
  <head>
      <meta charset="utf-8">
    <title> IsraSafe / ניהול</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/bootstrap/css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-rtl.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/cms_style.css') }}">
    
    <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
    <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
						
    
  </head>
  <body>
    
    <div class="container-fluid">
      
      <div class="row">
        <div class="col-md-12 cms-header">
          <h3 class="text-center">דף עריכה לאתר</h3>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12 cms-container">
          
          <div class="col-md-2 cms-main-nav">
            
            <ul class="nav nav-pills nav-stacked">
              
              <li><a href="{{ url('cms/dashboard') }}">Dashboard</a></li>
              <li><a href="{{ url('cms/menu') }}">דפים</a></li>
              <li><a href="{{ url('cms/content') }}">נתונים</a></li>
              <li><a href="{{ url('cms/categories') }}">קטגוריות</a></li>
              <li><a href="{{ url('cms/products') }}">מוצרים</a></li>
              <li><a href="{{ url('cms/sizes') }}">מידות</a></li>
              <li><a href="{{ url('cms/images') }}">תמונות</a></li>
              <li><a href="{{ url('cms/orders') }}">הזמנות</a></li>
              <li><a href="{{ url('cms/contacts') }}">יצירת קשר</a></li>
              <li><a href="{{ url('') }}">חזרה לאתר</a></li>
              <li><a href="{{ url('user/logout') }}">התנתק</a></li>
              
            </ul>
            
          </div>
          
          <div class="col-md-10">
            
            <div class="row">
              
              <div class="col-md-8 container">
                
                <br>
                
                @if(Session::has('sm')) @include('includes.sm')  @endif
                @if($errors->any()) @include('includes.errors') @endif
                @if(Session::has('em')) @include('includes.em')  @endif  
                
              </div>
              
            </div>
            
            <div class="row"> @yield('cms_content') </div>
              
          </div>
          
          <div class="col-md-1 cms-main-nav"></div>
          
        </div>
      </div>
      
      <hr class="hr">
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h6 class="h6"> All Rights To Or Tsury &copy; {{date('Y')}}</h6>
        </div>
    </div>
</div>
      
    </div>
    
    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/script.js') }}"></script>
    
  </body>
</html>

