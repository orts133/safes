@extends('cms.main')

@section('cms_content')

<div class="col-md-12">
  
  <h3>כאן עורכים את הדפים באתר</h3>
  
  <p><a class="btn btn-primary" href="{{ url('cms/menu/create') }}">+ Add new menu</a></p>
  <br>
  
  @if( count($main_menu) > 0 )
  
  <div class="col-md-6">
    <table class="table">

      <th>שם הלינק</th>
      <th>פעולה</th>

      @foreach($main_menu as $row)

      <tr>

        <td>{{ $row['link'] }}</td>

        <td>
          <a href="{{ url('cms/menu/' . $row['id'] . '/edit') }}">עורך</a> | 
          <a href="{{ url('cms/menu/' . $row['id']) }}">מחק</a>
        </td>

      </tr>

      @endforeach

    </table>
  </div>
  
  @endif
  
</div>

@endsection