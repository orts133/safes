
@extends('cms.main')

@section('cms_content')

  <div class="col-md-4">

    <h3>Add new content -  </h3>
    
    @if( count($main_menu) > 0 )

    <form method="post" action="{{ url('cms/content') }}">

      {!! csrf_field() !!}
      
      <input type="hidden" name="default_link" value="-1">

      <div class="form-group">
          <label for="title">Title:</label>
          <input value="{{ Input::old('title') }}" type="text" class="form-control" name="title"  placeholder="Title">
      </div>
      
      
      
      <div class="form-group">
          <label for="Link">Menu link:</label>
          
          <select name="link" class="form-control">
            
            <option value="-1">Choose menu...</option>
            
            @foreach($main_menu as $row)
            
            <option value="{{ $row['id']  }}">{{ $row['link'] }}</option>
            
            @endforeach
            
          </select>
          
      </div>

      <div class="form-group">
          <label for="body">Body:</label>
          <textarea name="body" class="form-control" rows="10">{{ Input::old('body') }}</textarea>
      </div>
      
      <div class="form-group">
        <label for="image">Content image:</label>
        <input type="file" name="image">
      </div>

      <input type="submit" name="submit" value="Add content" class="btn btn-primary">
      <a class="btn btn-default" href="{{ url('cms/content') }}">Cancel</a>

    </form>
    
    @else 
    
    <p><i>No menu links...</i></p>
    
    @endif

  </div>

@endsection