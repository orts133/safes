@extends('cms.main')

@section('cms_content')

<div class="col-md-12">
  
  <h3>כאן עורכים את התמונות באתר</h3>
  
  <p><a class="btn btn-primary" href="{{ url('cms/images/create') }}">הוספף מוצר חדש</a></p>
  <br>
  
  @if( count($images) > 0 )
  
  <div class="col-md-6">
    <table class="table img_table">
        <th>תמונה</th>
      <th>כותרת</th>
      <th>פעולה</th>

      @foreach($images as $row)

      <tr>
          <td>
              <img src="{{ asset('assets/img/' . $row['url']) }}" >
          </td>
          
        <td>{{ $row['title'] }}</td>

        <td>
           
          <a href="{{ url('cms/images/' . $row['id']) }}">מחק</a>
        </td>

      </tr>

      @endforeach

    </table>
  </div>
  
  @endif
  
</div>

@endsection