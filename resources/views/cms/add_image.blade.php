
@extends('cms.main')

@section('cms_content')

  <div class="col-md-4">

    <h3>הוסף תמונה חדשה -  </h3>
    
    @if( count($products) > 0 )

    <form method="post" action="{{ url('cms/images') }}" enctype="multipart/form-data">

      {!! csrf_field() !!}
      <input type="hidden" value="-1" name="default_categorie">

      <div class="form-group">
          <label for="title">כותרת:</label>
          <input value="{{ Input::old('title') }}" type="text" class="form-control" name="title"  placeholder="Title">
      </div>
      
      
      
      <div class="form-group">
        
        <label for="product">שם הדגם:</label>
        
        <select name="product" class="form-control">
          
          <option value="-1">בחר מוצר...</option>
          
          @foreach($products as $row)
          
            <option value="{{ $row['id'] }}">{{ $row['title'] }}</option>
          
          @endforeach
          
        </select>
        
      </div>
      

      
      <div class="form-group">
        <label for="image"> image:</label>
        <input type="file" name="image">
      </div>

      <input type="submit" name="submit" value="Add product" class="btn btn-primary">
      <a class="btn btn-default" href="{{ url('cms/images') }}">Cancel</a>
      <br><br><br><br>
    </form>
    
    @else
    
    <p><i>No images...</i></p>
    
    @endif
    

  </div>

@endsection