
@extends('cms.main')

@section('cms_content')

  <div class="col-md-4">

    <h3>ערוך מידה זו</h3>
    


    <form method="post" action="{{ url('cms/sizes/' . $size['id']) }}" enctype="multipart/form-data">

      {!! csrf_field() !!}
      
      <input type="hidden" name="_method" value="PATCH">
      <input type="hidden" name="id" value="{{ $size['id'] }}">
      <input type="hidden" value="-1" name="default_product">

      <div class="form-group">
          <label for="title">דגם:</label>
          <input value="{{ $size['title'] }}" type="text" class="form-control" name="title" >
      </div>
      
      <div class="form-group">
          <label for="o_height">גובה חוץ:</label>
          <input value="{{ $size['o_height'] }}" type="text" class="form-control" name="o_height" >
      </div>
      
      <div class="form-group">
          <label for="o_width">אורך חוץ:</label>
          <input value="{{ $size['o_height'] }}" type="text" class="form-control" name="o_width" >
      </div>
      
      <div class="form-group">
          <label for="o_depth">עומק חוץ:</label>
          <input value="{{ $size['o_height'] }}" type="text" class="form-control" name="o_depth" >
      </div>
      
      <div class="form-group">
          <label for="in_height">גובה פנים:</label>
          <input value="{{ $size['in_height'] }}" type="text" class="form-control" name="in_height" >
      </div>
      
      <div class="form-group">
          <label for="in_width">אורך פנים:</label>
          <input value="{{ $size['in_width'] }}" type="text" class="form-control" name="in_width" >
      </div>
      
      <div class="form-group">
          <label for="in_depth">עומק פנים:</label>
          <input value="{{ $size['in_width'] }}" type="text" class="form-control" name="in_depth" >
      </div>
      
      <div class="form-group">
          <label for="weight">משקל:</label>
          <input value="{{ $size['weight'] }}" type="text" class="form-control" name="weight" >
      </div>
      
      <div class="form-group">
        
        <label for="product_id">מוצר:</label>
        
        <select name="product_id" class="form-control">
            <option value="-1">בחר מוצר...</option>
                    
          @foreach($products as $row)
          
            <option value="{{ $row['id'] }}">{{ $row['title'] }}</option>
          
          @endforeach
          
        </select>
        
      </div>
      
      

      <input type="submit" name="submit" value="Save product" class="btn btn-primary">
      <a class="btn btn-default" href="{{ url('cms/sizes') }}">ביטול</a>
      <br><br><br><br>
    </form>
    
  </div>

@endsection