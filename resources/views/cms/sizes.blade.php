@extends('cms.main')

@section('cms_content')

<div class="col-md-12">
  
  <h3>דף עריכת מידות</h3>
  
  <p><a class="btn btn-primary" href="{{ url('cms/sizes/create') }}">הוספף מוצר חדש</a></p>
  <br>
  
  @if( count($sizes) > 0 )
  
  <div class="col-md-6">
    <table class="table">

      <th>כותרת</th>
      <th>פעולה</th>

      @foreach($sizes as $row)

      <tr>

        <td>{{ $row['title'] }}</td>

        <td>
          <a href="{{ url('cms/sizes/' . $row['id'] . '/edit') }}">ערוך</a> | 
          <a href="{{ url('cms/sizes/' . $row['id']) }}">מחק</a>
        </td>

      </tr>

      @endforeach

    </table>
  </div>
  
  @endif
  
</div>

@endsection