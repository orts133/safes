@extends('cms.main')

@section('cms_content')

<div class="col-md-12">
  
  <form action="{{ url('cms/products/' . $product_id) }}" method="post">
    
    <input type="hidden" name="_method" value="DELETE">
    
    {!! csrf_field() !!}
    
    <div class="form-group">
      <label>Are your sure you want to delete ?</label>
    </div>
    
     <input type="submit" name="submit" value="Delete" class="btn btn-primary">
     <a class="btn btn-default" href="{{ url('cms/products') }}">Cancel</a>
    
  </form>
  
</div>

@endsection