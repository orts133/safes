@extends('cms.main')

@section('cms_content')

<div class="col-md-12">
  
  <h3>דף ההזמנות</h3>
  
  <br>
  
  <div class="col-md-8">
    
  @if(count($contacts) > 0)
  
  <table class="table">
    
    <th>שם</th>
    <th>אימייל</th>
    <th>טלפון</th>
    <th>נושא</th>
    <th>הודעה</th>
    <th>תאריך</th>
    
    
    @foreach($contacts as $row)
    
    
    
    <tr>
      
      <td>{{ $row->name }}</td>
      <td>{{ $row->email }}</td>
      <td>{{ $row->phone }}</td>
      <td>{{ $row->subject }}</td>
      <td>{{ $row->message }}</td>
      <td>{{ $row->created_at }}</td>
       
    </tr>
    
    @endforeach
    
  </table>
  
  </div>
  
  @else
  
  <p><i>No contacts...</i></p>
  
  @endif
  
</div>

@endsection