@extends('cms.main')

@section('cms_content')

<div class="col-md-12">
  
  <h3>Here you can edit site content - </h3>
  
  <p><a class="btn btn-primary" href="{{ url('cms/content/create') }}">+ Add new content</a></p>
  <br>
  
  @if( count($content) > 0 )
  
  <div class="col-md-6">
    <table class="table">

      <th>Title</th>
      <th>Operation</th>

      @foreach($content as $row)

      <tr>

        <td>{{ $row['title'] }}</td>

        <td>
          <a href="{{ url('cms/content/' . $row['id'] . '/edit') }}">Edit</a> | 
          <a href="{{ url('cms/content/' . $row['id']) }}">Delete</a>
        </td>

      </tr>

      @endforeach

    </table>
  </div>
  
  @endif
  
</div>

@endsection