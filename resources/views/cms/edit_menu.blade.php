
@extends('cms.main')

@section('cms_content')

  <div class="col-md-4">

    <h3>Edit this menu -  </h3>

    <form method="post" action="{{ url('cms/menu/' . $menu['id']) }}">

      {!! csrf_field() !!}
      
      <input type="hidden" name="_method" value="PATCH">
      <input type="hidden" name="id" value="{{ $menu['id'] }}">
      
      <div class="form-group">
          <label for="link">Link:</label>
          <input value="{{ $menu['link'] }}" type="text" class="form-control" name="link"  placeholder="Link">
      </div>

      <div class="form-group">
          <label for="title">Title:</label>
          <input value="{{ $menu['title'] }}" type="text" class="form-control" name="title"  placeholder="Title">
      </div>

      <input type="submit" name="submit" value="Save menu" class="btn btn-primary">
      <a class="btn btn-default" href="{{ url('cms/menu') }}">Cancel</a>

    </form>

  </div>

@endsection