@extends('cms.main')

@section('cms_content')

<div class="col-md-12">
  
  <h3>Here you can edit site categories - </h3>
  
  <p><a class="btn btn-primary" href="{{ url('cms/categories/create') }}">+ Add new category</a></p>
  <br>
  
  @if( count($categories) > 0 )
  
  <div class="col-md-6">
    <table class="table">

      <th>Title</th>
      <th>Operation</th>

      @foreach($categories as $row)

      <tr>

        <td>{{ $row['title'] }}</td>

        <td>
          <a href="{{ url('cms/categories/' . $row['id'] . '/edit') }}">Edit</a> | 
          <a href="{{ url('cms/categories/' . $row['id']) }}">Delete</a>
        </td>

      </tr>

      @endforeach

    </table>
  </div>
  
  @endif
  
</div>

@endsection