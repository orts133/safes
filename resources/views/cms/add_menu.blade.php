
@extends('cms.main')

@section('cms_content')

  <div class="col-md-4">

    <h3>Add new menu -  </h3>

    <form method="post" action="{{ url('cms/menu') }}">

      {!! csrf_field() !!}

      <div class="form-group">
          <label for="link">Link (Menu button):</label>
          <input value="{{ Input::old('link') }}" type="text" class="form-control" name="link"  placeholder="Link">
      </div>

      <div class="form-group">
          <label for="title">Title:</label>
          <input value="{{ Input::old('title') }}" type="text" class="form-control" name="title"  placeholder="Title">
      </div>

      <input type="submit" name="submit" value="Add menu" class="btn btn-primary">
      <a class="btn btn-default" href="{{ url('cms/menu') }}">Cancel</a>

    </form>

  </div>

@endsection