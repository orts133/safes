
@extends('cms.main')

@section('cms_content')

  <div class="col-md-4">

    <h3>ערוך מוצר זה</h3>
    


    <form method="post" action="{{ url('cms/products/' . $product['id']) }}" enctype="multipart/form-data">

      {!! csrf_field() !!}
      
      <input type="hidden" name="_method" value="PATCH">
      <input type="hidden" name="id" value="{{ $product['id'] }}">
      <input type="hidden" value="-1" name="default_categorie">

      <div class="form-group">
          <label for="title">Title:</label>
          <input value="{{ $product['title'] }}" type="text" class="form-control" name="title"  placeholder="Title">
      </div>
      
      
      
      <div class="form-group">
        
        <label for="categorie">Categorie:</label>
        
        <select name="categorie" class="form-control">
            <option value="-1">Chooce categorie...</option>
                    
          @foreach($categories as $row)
          
            <option value="{{ $row['id'] }}">{{ $row['title'] }}</option>
          
          @endforeach
          
        </select>
        
      </div>
      
      <div class="form-group">
          <label for="article">Preview article:</label>
          <textarea name="preview" class="form-control" rows="5">{{ $product['preview'] }}</textarea>
      </div>
      
      <div class="form-group">
          <label for="article">Article:</label>
          <textarea name="article" class="form-control" rows="10">{{ $product['article'] }}</textarea>
      </div>
      
      <div class="form-group">
        <label for="file">Product image:
        <img border="0" width="60" src="{{ asset('assets/img/' . $product['image']) }}">
        </label>
        <input type="file" name="image">
      </div>

      <input type="submit" name="submit" value="Save product" class="btn btn-primary">
      <a class="btn btn-default" href="{{ url('cms/products') }}">Cancel</a>
      <br><br><br><br>
    </form>
    
  </div>

@endsection