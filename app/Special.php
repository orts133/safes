<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Special extends Model{
    
    static public function get_specials(&$data){
      
      $data['specials']= [] ;

      $specials = self::all()->toArray();
          foreach ($specials as $special){
              
              if($product = Product::where('id' , '=' , $special['product_id'] )->first()){                 
              $product = $product->toArray();
              $categorie = Categorie::where('id' , '=' , $product['categorie_id'])->first()->toArray();
              $categorie = $categorie['url'];
              $product['categorie'] = $categorie;
              $product['old_price'] = $special['old_price'];
              array_push($data['specials'], $product);
              }
          }     
  }
  
  static public function edit_specials($request, $id){
      
  }
}
