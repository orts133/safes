<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Input;
use Session;

class Product extends Model{
    
  public function images(){
        
        return $this->hasMany('app\Image');
    }
  public function sizes(){
        
        return $this->hasMany('app\Size');
    }
  
  static public function get_item($product, &$data){
    
    $data['product'] = null;
    
    if( $product = self::where('url' , '=', $product)->first() ){
      
      $data['product'] = $product->toArray();
      
      $data['title'] = $data['title'] . '- ' .$data['product']['title'];
      
    }
    
  }

  static public function get_products($categorie, &$data){
    
    $data['products'] = [];
    
  
    
    if( $categorie = Categorie::where('url', '=', $categorie)->first() ){
   
      
      $categorie = $categorie->toArray();
      
      if( $products = Categorie::find( $categorie['id'] )->products ){
        
        $data['products'] = $products->toArray();
        $data['title'] = $data['title'] . '- ' .$categorie['title'];
        $data['categorie'] = $categorie['url'];
      }       
    }
  }
  

    static public function latests($latests, &$data){
      $data['latests']= [] ;
      
      if($latests){
          
          foreach ($latests as $latest){
              
             if( $item = Product::where('id' , '=' , $latest )->first()){
              $item = $item->toArray();
                if($categorie = Categorie::where('id' , '=' , $item['categorie_id'])->first()){
                    $categorie = $categorie->toArray();
                    $categorie = $categorie['url'];
                    $item['categorie'] = $categorie;
                    array_push($data['latests'], $item);
                }
              
             }
          }
      }
     
      
  }
   
  
    static public function add_product($request){
        
        $file_name='';
        
        if( Input::hasFile('image') && Input::file('image')->isValid() ){
            $file = Input::file('image');
            $file_name = str_random(30) . '.' . $file->getClientOriginalExtension();
            $file->move( public_path() . '/assets/img/', $file_name);
          }else{
              Session::flash('em', 'No image has been upload! check product details');
          }
        $product = new Product();
        $product->title = $request['title'];
        $product->categorie_id = $request['categorie'];
        $product->preview = $request['preview'];
        $product->article = $request['article'];
        $product->image =!empty($file_name) ?  $file_name : 'default.jpg';
        $product->url = General::make_url($request['title']);
        $product->save();
        Session::flash('sm' , 'New product has been saved!');
    }
    
    static public function edit_product($request, $id){
        $file_name='';
        
        if( Input::hasFile('image') && Input::file('image')->isValid() ){
            $file = Input::file('image');
            $file_name = str_random(10) . '.' . $file->getClientOriginalExtension();
            $file->move( public_path() . '/assets/img/', $file_name);
          }else{
              Session::flash('em', 'No image has been upload! check product details');
          }
        
        $product = Product::find($id);
        $product->title = $request['title']; 
        $product->categorie_id = $request['categorie'];
        $product->preview = $request['preview'];
        $product->article = $request['article'];
        if(!empty($file_name)){
            $product->image =$file_name;
        }
        $product->url = General::make_url($request['title']);
        $product->save();
        Session::flash('sm' , 'Product has been Updated!');
    }
  
}
