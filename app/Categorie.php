<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Input;
use Session;

class Categorie extends Model{
    
    public function products(){
        
        return $this->hasMany('app\Product');
    }
    
    static public function get_categories(&$data){
        
        return $data['categories'] =  self::all()->toArray();
    }
    
    static public function add_categorie($request){
        $file_name='';
        
        if( Input::hasFile('image') && Input::file('image')->isValid() ){
            $file = Input::file('image');
            $file_name = str_random(10) . '.' . $file->getClientOriginalExtension();
            $file->move( public_path() . '/assets/img/', $file_name);
          }else{
              Session::flash('em', 'No image has been upload! check product details');
          }
        
        $categorie= new Categorie();
        $categorie->title = $request['title'];
        $categorie->article = $request['article'];
        $categorie->image =!empty($file_name) ?  $file_name : 'default.jpg';
        $categorie->url = General::make_url($request['title']);
        $categorie->save();
        Session::flash('sm' , 'New categorie has been saved!');
    }
    
    static public function edit_categorie($request, $id){
        $file_name='';
        
        if( Input::hasFile('image') && Input::file('image')->isValid() ){
            $file = Input::file('image');
            $file_name = str_random(10) . '.' . $file->getClientOriginalExtension();
            $file->move( public_path() . '/assets/img/', $file_name);
          }else{
              Session::flash('em', 'No image has been upload! check product details');
          }
        
        $categorie = Categorie::find($id);
        $categorie->title = $request['title'];
        $categorie->article = $request['article'];
        if(!empty($file_name)){
            $categorie->image =$file_name;
        }
        $categorie->url = General::make_url($request['title']);
        $categorie->save();
        Session::flash('sm' , 'Categorie has been Updated!');
    }
}
