<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class General extends Model{
    
    static public function make_url($str){
        
        $str = trim($str);
        $str = strtolower($str);
        $str = str_replace(' ' , '-' , $str);
        return $str;
    }
}
