<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Cart;
use Session;
use App\Size;

class Store extends Model {
  
  static public function cart_add($id){
    
      if ($size = Size::find($id)){
    $size = $size->toArray();
    $product = Product::find($size['product_id'])->toArray();
    Cart::add($id, $product['title'],0, 1,array(
        $size['title'], $size['o_height'], $size['o_width'], $size['o_depth'],
        $size['in_height'], $size['in_width'], $size['in_depth'], $size['weight']
            ));

    Session::flash('sm', $product['title'] .', ' . $size['title'] . ', נוסף לעגלה!');
      }
      
  }
 
   static public function get_offer($id, &$data){

        $data['size'] = null;
        $data['product'] = null;
      if ($size = Size::find($id)){
        $size = $size->toArray();
        $product = Product::find($size['product_id'])->toArray();
        $categorie = Categorie::find($product['categorie_id'])->toArray();
        
        $data['size'] = $size;
        $data['product'] = $product;
        $data['categorie'] = $categorie;
      }
   }
  
  
  static public function  cart_update($input){
      if(!empty($input['id']) && !empty($input['op'] && $product = Cart::get($input['id']))){
          if($input['op'] == 'plus'){
              
              Cart::update($input['id'],['quantity' => 1]);
              
          }elseif($input['op']== 'minus') {
                if($product['quantity'] - 1 == 0){

                    Cart::remove($input['id']);
                }   else{
                
                         Cart::update($input['id'], ['quantity' => -1]);
                        }   
                  
                  
            }elseif($input['op'] == 'remove'){
                Cart::remove($input['id']);
            }
        }
    }
 
}

