<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use Cart;
use DB;
use App\Size;
use Mail;

class Order extends Model{
   
    static public function save_order($request){
        $cartCollection=Cart::getContent();
        $order = new Order();
        $order->name= $request['name'];
        $order->email= $request['email'];
        $order->phone= $request['phone'];
        $order->fax= $request['fax'];
        $order->data= $cartCollection->toJson();
        $order->save();
        Cart::clear();
        Mail::send('mails.NewOrder', ['name' => $request['name'], 'email' => $request['email'], 'phone' => $request['phone'], 'fax' => $request['fax'], 'orders' => $cartCollection, ], function($message){
            
            $message->to('tsuryor@gmail.com')->subject('הזמנת הצעת מחיר חדשה מהאתר');
        });
        Session::flash('sm', 'תודה שבחרת IsraSafe , נהיה בקשר ביום העסקים הקרוב!');
    }
    
    static public function get_orders(){
        $sql="SELECT o.*  FROM orders o ORDER BY o.created_at DESC";
        return DB::select($sql);
        
    }
    
    static public function save_dataorder($request){
        Cart::clear();
        $size = Size::find($request['id']);
        $size = $size->toArray();
        $product = Product::find($size['product_id']);
        $product = $product->toArray();
        Cart::add($size['id'],$product['title'] , 0 , 1 , array(0=>$size['title']));
         $cartCollection=Cart::getContent();
        $order = new Order();
        $order->name= $request['name'];
        $order->email= $request['email'];
        $order->phone= $request['phone'];
        $order->fax= $request['fax'];
        $order->data= $cartCollection->toJson();
        Cart::clear();
        $order->save();
        Mail::send('mails.NewOrder', ['name' => $request['name'], 'email' => $request['email'], 'phone' => $request['phone'], 'fax' => $request['fax'], 'orders' => $cartCollection, ], function($message){
            
            $message->to('tsuryor@gmail.com')->subject('הזמנת הצעת מחיר חדשה מהאתר');
        });
        Session::flash('sm', 'תודה שבחרת IsraSafe ! נהיה בקשר ביום עסקים הקרוב');
    }
    
}
