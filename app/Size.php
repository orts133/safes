<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class Size extends Model{
    
     static public function get_sizes($product, &$data){
        
        $data['sizes'] = null;
    
        if( $product = Product::where('url' , '=', $product)->first() ){
      
        $product = $product->toArray();
            if($sizes = Product::find($product['id'])-> sizes){
      
            $data['sizes'] = $sizes->toArray();
         
            }
        }
        
    }
    
    static public function add_size($request){
        
        $size = new Size();
        $size->title = $request['title'];
        $size->product_id = $request['product_id'];
        $size->o_height = $request['o_height'];
        $size->o_width = $request['o_width'];
        $size->o_depth = $request['o_depth'];
        $size->in_height = $request['in_height'];
        $size->in_width = $request['in_width'];
        $size->in_depth = $request['in_depth'];
        $size->weight = $request['weight'];
        $size->save();
        Session::flash('sm' , 'מידה חדשה נוצרה!');
        
    }
    
    static public function edit_size($request, $id){
        $size = Size::find($id);
        $size->title = $request['title'];
        $size->o_height = $request['o_height']; 
        $size->o_width = $request['o_width']; 
        $size->o_depth = $request['o_depth']; 
        $size->in_height = $request['in_height']; 
        $size->in_width = $request['in_width']; 
        $size->in_depth = $request['in_depth']; 
        $size->weight = $request['weight']; 
        $size->product_id = $request['product_id'];
        $size->save();
        Session::flash('sm' , 'מידה עודכנה !');
    }
}
