<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Hash;
use Session;

class User extends Model {
    
    static public function checkUser($email,$password){
        $valid=false;
        
        $sql = "SELECT u.* , r.role FROM users u JOIN roles r ON u.id=r.user_id WHERE email=?";
        
        $user = DB::select($sql,[$email]);
        if(count($user) >0){
            if(Hash::check($password, $user[0]->password)){
                Session::set('user_id', $user[0]->id);
                Session::set('user_name', $user[0]->name);
                if($user[0]->role == 1){
                    
                    Session::set('is_admin', true);
                }
                Session::flash('sm', 'Welcome ' . $user[0]->name . ', you are now logged in!' );
                 $valid=true;
                
            }
            
        }
        return $valid;
        
    }
    
    static public function saveUser($request){
        
        $user = new User();
        $user->name=$request['fname'] . ' ' . $request['lname']; 
        $user->email=$request['email'];
        $user->password= bcrypt($request['password']);
        $user->save();
        DB::insert("INSERT INTO roles VALUES(?,2)",[$user->id]);
        Session::flash('sm', 'Register complete! Enter your accout to continue');
    }
}
