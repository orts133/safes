<?php namespace App;

use Session;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model{
    
    public function contents() {
    return $this->hasMany('App\Content');
    }
    
    static public function add_menu($request){
      
        $menu = new Menu();
        $menu->link = $request['link'];
        $menu->title = $request['title'];
        $menu->url = General::make_url($request['title']);
        $menu->save();
        Session::flash('sm' , 'New Menu has been saved!');
    }
    
    static public function edit_menu($request, $id){
      
        $menu = Menu::find($id);
        $menu->link = $request['link'];
        $menu->title = $request['title'];
        $menu->url = General::make_url($request['title']);
        $menu->save();
        Session::flash('sm' , 'Menu has been updated!');
    }
}
