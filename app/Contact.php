<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Mail;

class Contact extends Model{
    static public function save_contact($request){
        
        $contact = new Contact();
        $contact->name= $request['name'];
        $contact->email= $request['email'];
        $contact->phone= $request['phone'];
        $contact->subject= $request['subject'];
        $contact->message= $request['message'];
        $contact->save();
        Mail::send('mails.NewContact', ['name' => $request['name'], 'email' => $request['email'], 'phone' => $request['phone'], 'subject' => $request['subject'], 'bodyMessage' => $request['message'], ], function($message){
            
            $message->to('tsuryor@gmail.com')->subject('בקשה ליצור קשר מהאתר');
        });
        Session::flash('sm' , 'תודה שיצרת עימנו קשר, נהיה בקשר ביום עסקים הקרוב.');
    }
    
    static public function get_contacts(){
        $sql="SELECT c.*  FROM contacts c ORDER BY c.created_at DESC";
        return DB::select($sql);
        
    }
   
}
