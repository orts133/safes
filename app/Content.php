<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use Input;
use Session;

class Content extends Model{
    
    static public function get_content($page, &$data){
    
    $data['content'] = [];
    
    if($menu = Menu::where('url', '=', $page)->first()){
      
      $menu = $menu->toArray();
      $content = Menu::find($menu['id'])->contents;
      $data['content'] = $content->toArray();
      
        }
    
    }
    
    static public function add_content($request){
        
        $file_name='';
        
        if( Input::hasFile('image') && Input::file('image')->isValid() ){
            $file = Input::file('image');
            $file_name = str_random(30) . '.' . $file->getClientOriginalExtension();
            $file->move( public_path() . '\assets\img', $file_name);
          }
        $content = new Content();
        $content->menu_id = $request['link'];
        $content->title = $request['title'];
        $content->body = $request['body'];
        
        if(!empty($file_name)){
            $content->image = $file_name;
        }

        $content->save();
        Session::flash('sm' , 'New Content has been saved!');
    }
    
    static public function edit_content($request, $id){
        $file_name='';
        
        if( Input::hasFile('image') && Input::file('image')->isValid() ){
            $file = Input::file('image');
            $file_name = str_random(10) . '.' . $file->getClientOriginalExtension();
            $file->move( public_path() . '\assets\img', $file_name);
          }
        
        $content = Content::find($id);
        $content->title = $request['title'];
        $content->menu_id = $request['link'];
        $content->body = $request['body'];

        if(!empty($file_name)){
            $content->image =$file_name;
        }
        $content->save();
        Session::flash('sm' , 'Content has been Updated!');
    }
}
