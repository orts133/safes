<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use Input;
use Session;

class Image extends Model{
    
    static public function get_images($product, &$data){
        
        $data['images'] = null;
    
    if( $product = Product::where('url' , '=', $product)->first() ){
      
      $product = $product->toArray();
        if($images = Product::find($product['id'])-> images){
      
         $data['images'] = $images->toArray();
         
        }
     }
        
    }
    
    
    static public function add_Image($request){
        
        $file_name='';
        
        if( Input::hasFile('image') && Input::file('image')->isValid() ){
            $file = Input::file('image');
            $file_name = str_random(20) . '.' . $file->getClientOriginalExtension();
            $file->move( public_path() . '/assets/img/', $file_name);
            $image = new Image();
            $image->title = $request['title'];
            $image->product_id = $request['product'];
            $image->url = $file_name;
            $image->save();
            Session::flash('sm' , 'New Image has been saved!');
          }else{
              Session::flash('em', 'No image has been upload! check product details');
          }
        
    }
    
}
