<?php namespace App\Http\Middleware;

use Closure;
use Session;

class VerifyAdmin{
    
    public function handle($request, Closure $next){
       if( ! Session::has('is_admin') ){
      
            return redirect('user/login');

          } else {

            return $next($request);

          }
    
  }
    
}
