<?php namespace App\Http\Middleware;
use Session;
use Closure;

class verifyUser
{
    public function handle($request, Closure $next)
    {
        if(Session::has('user_id')){
        return redirect('');
        }
        return $next($request);
    }
}
