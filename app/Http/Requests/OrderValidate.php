<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrderValidate extends Request{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|min:2',
            'email' => 'required|email',
            'phone' => 'required|min:9',
            'fax' => 'min:9',
        ];
    }
    
    public function messages() {
        
        return [
            'name.required' => 'יש להזין שם.',            
            'email.required' => 'יש להזין אימייל.',
            'phone.required' => 'יש להזין טלפון.',
            'name.min' => 'שם עם 2 אותיות לפחות.',
            'email.email' => 'יש להזין אימייל תקין.',
            'phone.min' => 'יש להזין טלפון תקין.',
        ];
    }
}
