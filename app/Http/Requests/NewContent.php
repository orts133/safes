<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewContent extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|min:2',
            'link' => 'required',
            'body' => 'required|min:2',
        ];
    }
}
