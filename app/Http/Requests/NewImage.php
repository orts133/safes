<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewImage extends Request{
    
    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'product' => 'required',
            'title' => 'required|min:2',
            'image' => 'required',

        ];
    }
}
