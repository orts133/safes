<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewUserValidate extends Request{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:5',
            'repassword' => 'required|same:password',
        ];
    }
    
    public function messages() {
        
        return [
            'fname.required' => 'First name is required!',
            
            'lname.required' => 'Last name is required!',
        ];
    }
}
