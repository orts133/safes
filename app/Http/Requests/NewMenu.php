<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewMenu extends Request{
   
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'link' => 'required|min:2',
            'title' => 'required|min:2',
        ];
    }
}
