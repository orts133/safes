<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewSize extends Request{
   
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'product_id' => 'required',
            'title' => 'required',
            'o_height' => 'required',
            'o_width' => 'required',
            'o_depth' => 'required',
            'in_height' => 'required',
            'in_width' => 'required',
            'in_depth' => 'required',
            'weight' => 'required',
            
        ];
    }
}
