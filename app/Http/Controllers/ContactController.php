<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Http\Requests\VerifyContact;
use Illuminate\Support\Facades\Redirect;

class ContactController extends MainController{
    
    public function contact_save(VerifyContact $request){
        if($request){
        Contact::save_contact($request);
        return Redirect::back();
        }    
    }
    
    public function get_contacts(){
        self::$data['contacts'] = Contact::get_contacts();
        return view('cms.contacts', self::$data);
    }
}
