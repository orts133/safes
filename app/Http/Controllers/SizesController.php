<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\NewCategorie;
use App\Http\Requests\NewProduct;
use App\Http\Controllers\Controller;
use App\Categorie;
use App\Product;
use App\Size;
use Session;
use App\Http\Requests\NewSize;

class SizesController extends CmsController{

    public function index(){
        self::$data['sizes'] = Size::all()->toArray();
        return view('cms.sizes', self::$data);
    }

    public function create(){
        self::$data['products']= Product::all()->toArray();
        return view('cms.add_size', self::$data);
    }

    public function store(NewSize $request ){
        Size::add_size($request);
        return redirect('cms/sizes');
    }

    public function show($id){
        self::$data['size_id'] = $id;
        return view('cms.delete_size' , self::$data);
    }

    public function edit($id){
        self::$data['products']= Product::all()->toArray();
        self::$data['size'] = Size::where('id','=' ,$id)->first()->toArray();
        return view('cms.edit_size', self::$data);
    }

    public function update(NewSize $request, $id){
        Size::edit_size($request, $id);
        return redirect('cms/sizes');
    }

    public function destroy($id){
        Size::destroy($id);
        Session::flash('sm', 'מידה נמחקה !');
        return redirect('cms/sizes');
    }
}
