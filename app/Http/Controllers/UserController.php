<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserValidate;
use App\Http\Requests\NewUserValidate;
use App\Http\Controllers\Controller;
use App\User;
use Session;
use Input;


class UserController extends MainController {
    
    function __construct() {
        parent::__construct();
        $this->middleware('user', ['except' => ['getLogout', 'getEdit']]);
    }

    public function getLogin(){
    self::$data['title']= self::$data['title'] . '- Login';
    self::$data['ds'] = Input::get('ds');
    return view('forms.login', self::$data);    
    }
    
    public function getRegister(){
    self::$data['title']= self::$data['title'] . '- Register';    
    return view('forms.register', self::$data);
    }
    
    public function postLogin(UserValidate $request){
      
      if(  User::checkUser($request['email'] , $request['password'])){
          return $request['ds'] ? redirect($request['ds']) : redirect('');
      }else{
          Session::flash('em', 'Email / Password was incorrect!');
          return redirect('user/login');
      }
    }
    
    public function postRegister(NewUserValidate $request){
        User::saveUser($request);
        return redirect('user/login');
    }
    
    public function getLogout(){
        
        Session::forget('user_id');
        Session::forget('user_name');
        if(Session::has('is_admin')){
            Session::forget('is_admin');
        }
        return redirect('');
    }
    
}
