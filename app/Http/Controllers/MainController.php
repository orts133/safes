<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Categorie;
use Cart;
use App\Menu;

class MainController extends Controller {
     static public $data = ['title' => 'IsraSafe'];
    
     public function __construct() {
         self::$data['main_menu'] = Menu::all()->toArray();
         self::$data['email'] = "israsafe100@gmail.com";
         self::$data['cellphone'] = "052-6606367";
         self::$data['jobphone'] = "03-6701378";
         self::$data['jobfax'] = "03-6701378";
         self::$data['address'] = "שילה 11, רמת גן";
         self::$data['cart'] = null;
        Categorie::get_categories(self::$data);
        if(Cart::getTotalQuantity()>0){
         self::$data['cart']=Cart::getContent();
        
        }
    }
     
}
