<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Http\Requests\NewMenu;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class MenuController extends CmsController {

    public function index(){
        self::$data['main_menu'] = Menu::all()->toArray();
        return view('cms.menu', self::$data);
    }

    public function create(){
       
        return view('cms.add_menu');  
    }

    public function store(NewMenu $request){
        Menu::add_menu($request);
        return redirect('cms/menu');
    }
    
    public function show($id){
        self::$data['menu_id'] = $id;
        return view('cms.delete_menu' , self::$data);       
    }
    
    public function edit($id){
        self::$data['menu'] = Menu::where('id','=' ,$id)->first()->toArray();
        return view('cms.edit_menu', self::$data);
    }

    public function update(NewMenu $request, $id){
        Menu::edit_menu($request, $id);
        return redirect('cms/menu');
    }

    public function destroy($id){
        Menu::destroy($id);
        Session::flash('sm', 'Menu link has been deleted!');
        return redirect('cms/menu');
    }
}
