<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorie;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Content;
use App\Product;
use App\Special;
use Session;
use App\Http\Requests\VerifyContact;
use Mail;

class PagesController extends MainController{
    public function index(){
        $latests =array(10,11,13,15,17,16,19,12, 14);
        Product::latests($latests, self::$data);
        Special::get_specials(self::$data);
        self::$data['title'] = self::$data['title'] . '- דף הבית';
        return view('content.home', self::$data);
    }
   
    
    public function boot($page){
    Content::get_content($page, self::$data);
    self::$data['title']= self::$data['title'] . '- ' . $page;
    return view('content.boot', self::$data); 
    }

    
}
