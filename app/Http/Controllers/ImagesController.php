<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\NewImage;

use App\Http\Controllers\Controller;
use App\Image;
use App\Product;
use Session;

class ImagesController extends CmsController{

    public function index(){
        self::$data['images'] = Image::all()->toArray();
        return view('cms.images', self::$data);
    }

    public function create(){
        self::$data['products']= Product::all()->toArray();
        return view('cms.add_image', self::$data);
    }

    public function store(NewImage $request ){
        Image::add_image($request);
        return redirect('cms/images');
    }

    public function show($id){
        self::$data['image_id'] = $id;
        return view('cms.delete_image' , self::$data);
    }

  

   

    public function destroy($id){
        Image::destroy($id);
        Session::flash('sm', 'Image has been deleted!');
        return redirect('cms/images');
    }
}
