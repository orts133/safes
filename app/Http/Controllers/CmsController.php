<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;

class CmsController extends MainController{
    
    function __construct() {
      parent::__construct();
      $this->middleware('cms', ['except' => ['']]);
    }

    public function getOrders(){
        self::$data['orders'] = Order::get_orders();
        return view('cms.orders', self::$data);
    }
    
    
}
