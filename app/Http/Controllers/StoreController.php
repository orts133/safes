<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Categorie;
use App\Product;
use App\Store;
use App\Order;
use Input;
use Cart;
use Session;
use Redirect;
use App\Image;
use App\Size;
use App\Http\Requests\OrderValidate;
use Mail;

class StoreController extends MainController {
  
  public function index(){
   self::$data['title'] = self::$data['title']  . '- ' . 'החנות שלנו';
    return view('content.store', self::$data);   
    
  }
  
  public function products($categorie){ 
    Product::get_products($categorie, self::$data);
    return view('content.products', self::$data);
  }
  
  public function item($categorie, $product){
    Product::get_item($product, self::$data);
    Image::get_images($product, self::$data);
    Size::get_sizes($product, self::$data);
    self::$data['categorie'] = $categorie;
    self::$data['size'] = null;

    return view('content.item', self::$data);
    
  }
  
  public function add_to_cart(){
    Store::cart_add( Input::get('id') ); 
    
  }
  
  public function get_offer(){
      self::$data['size'] = null;
    //Store::get_offer(Input::get('id'), self::$data);
      $id=Input::get('id');
    if($size = Size::find($id)){
       $size = $size->toArray(); 
       self::$data['size'] = $size;

       return json_encode(self::$data);
    }
    

  }
  
  public function checkout(){
    $cart = Cart::getContent();
    $cart = $cart->toArray();
    sort($cart);
    self::$data['cartCollection'] = $cart;
    self::$data['title'] = self::$data['title'] . '- ' . 'עגלת קניות';
    return view('content.checkout', self::$data);

  }
  
  public function update_cart(){
      
    $input= Store::cart_update(Input::all() );  
  }
  
  public function clear_cart(){
    Cart::clear();
    return Redirect::back();

  }
    public function order(OrderValidate $request){
        
        if(Cart::isEmpty()){
            return redirect('store/checkout');
        }
        if($request){

            Order::save_order($request);
            return redirect('');
        }


    }
    
    public function save_dataorder(OrderValidate $request){
        
        if($request){
           
            Order::save_dataorder($request);
            return Redirect::back();
        }else{
            return Redirect::back();
        }
    }


    public function cart(){
      $cartCollection = Cart::getContent();
    $cart = $cartCollection->toArray();
    sort($cart);
    self::$data['cartCollection'] = $cartCollection->toArray();
    return view('content.cart', self::$data);
      
  }
}
