<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends MainController{
    
   public function index(){

        return view('cms.dashboard', self::$data);
    }
}
