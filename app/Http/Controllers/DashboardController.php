<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;

class DashboardController extends CmsController{
    
    public function index(){
        
        self::$data['products'] = Product::all()->toArray();
        return view('cms.dashboard', self::$data);
    }

       
}
