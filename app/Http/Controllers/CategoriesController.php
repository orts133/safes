<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\NewCategorie;
use App\Http\Controllers\Controller;
use App\Categorie;
use Session;

class CategoriesController extends CmsController{

    public function index(){
        self::$data['categories'] = Categorie::all()->toArray();
        return view('cms.categories', self::$data);
    }

    public function create(){
      
        return view('cms.add_categorie');
    }

    public function store(NewCategorie $request){
        Categorie::add_categorie($request);
        return redirect('cms/categories');
    }

    public function show($id){
        self::$data['categorie_id'] = $id;
        return view('cms.delete_categorie' , self::$data);
    }

    public function edit($id){
        self::$data['categorie'] = Categorie::where('id','=' ,$id)->first()->toArray();
        return view('cms.edit_categorie', self::$data);
    }

    public function update(NewCategorie $request, $id){
        Categorie::edit_categorie($request, $id);
        return redirect('cms/categories');
    }

    public function destroy($id){
        Categorie::destroy($id);
        Session::flash('sm', 'Categorie has been deleted!');
        return redirect('cms/categories');
    }
}
