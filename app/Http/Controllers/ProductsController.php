<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\NewCategorie;
use App\Http\Requests\NewProduct;
use App\Http\Controllers\Controller;
use App\Categorie;
use App\Product;
use Session;

class ProductsController extends CmsController{

    public function index(){
        self::$data['products'] = Product::all()->toArray();
        return view('cms.products', self::$data);
    }

    public function create(){
        self::$data['categories']= Categorie::all()->toArray();
        return view('cms.add_product', self::$data);
    }

    public function store(NewProduct $request ){
        Product::add_product($request);
        return redirect('cms/products');
    }

    public function show($id){
        self::$data['product_id'] = $id;
        return view('cms.delete_product' , self::$data);
    }

    public function edit($id){
        self::$data['categories']= Categorie::all()->toArray();
        self::$data['product'] = Product::where('id','=' ,$id)->first()->toArray();
        return view('cms.edit_product', self::$data);
    }

    public function update(NewProduct $request, $id){
        Product::edit_product($request, $id);
        return redirect('cms/products');
    }

    public function destroy($id){
        Product::destroy($id);
        Session::flash('sm', 'Product has been deleted!');
        return redirect('cms/products');
    }
}
