<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\Menu;
use App\Content;
use App\Http\Requests\NewContent;

class ContentController extends CmsController
{
    public function index(){
        self::$data['content'] = Content::all()->toArray();
        return view('cms.content', self::$data);
    }

    public function create(){
        self::$data['main_menu']= Menu::all()->toArray();
        return view('cms.add_content', self::$data); 
    }

    public function store(NewContent $request){
        Content::add_content($request);
        return redirect('cms/content');
    }

    public function show($id){
        self::$data['content_id'] = $id;
        return view('cms.delete_content' , self::$data);
    }

    public function edit($id){
        self::$data['menu']= Menu::all()->toArray();
        self::$data['content'] = Content::where('id','=' ,$id)->first()->toArray();
        return view('cms.edit_content', self::$data);
    }

    public function update(NewContent $request, $id){
        Content::edit_Content($request, $id);
        return redirect('cms/content');
    }

    public function destroy($id){
        Content::destroy($id);
        Session::flash('sm', 'Content has been deleted!');
        return redirect('cms/content');
    }
}
