<?php

#pages
Route::get('/', 'PagesController@index');


#users
Route::controller('user', 'UserController');


#store
Route::get('store', 'StoreController@index');
Route::get('store/checkout', 'StoreController@checkout');
Route::get('store/cart', 'StoreController@checkout');
Route::get('store/add_to_cart', 'StoreController@add_to_cart');
Route::get('store/update_cart', 'StoreController@update_cart');
Route::get('store/clear_cart', 'StoreController@clear_cart');
Route::get('store/get_offer', 'StoreController@get_offer');
Route::post('store/order', 'StoreController@order');
Route::post('store/dataorder', 'StoreController@save_dataorder');
Route::get('store/{categorie}', 'StoreController@products');
Route::get('store/{categorie}/{product}', 'StoreController@item');

#contact
Route::post('contact' , 'ContactController@contact_save');

#cms
Route::resource('cms/categories', 'CategoriesController');
Route::resource('cms/products', 'ProductsController');
Route::resource('cms/menu', 'MenuController');
Route::resource('cms/content', 'ContentController');
Route::resource('cms/dashboard', 'DashboardController');
Route::resource('cms/contacts', 'ContactController@get_contacts');
Route::resource('cms/sizes', 'SizesController');
Route::resource('cms/images', 'ImagesController');
Route::controller('cms', 'CmsController');

#contact
Route::get('contact/save', 'ContactController@contact_save');


#menu - always at the end
Route::get('{page}', 'PagesController@boot');

